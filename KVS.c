#include "myIPC.h"
#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include "avl.h"

#define DEBUG 1 //Set to 1 to get debug output.

//Everybody will hate me for that:
#define DEBUG_PRINT(fmt, ...) \
if(DEBUG) { \
	printf(fmt, __VA_ARGS__); \
}

// svn sucks. >> git is at https://bitbucket.org/luckydonald/betriebssys-aufg-7/

static struct avl_node_t * root = NULL; //given.

static void cleanup(void * data, void * args); //belongs int the header...
                                               //but that would be KVS.h,
                                               //not KVS.c, which is the only file allowed to edit...

static void print_data(void * data, void * args); //same.

static uint32_t * get_value_by_key(uint32_t key){
    DEBUG_PRINT("Getting value by key '%d'", key);
    if (root == NULL) {
        printf("Empty Tree!\n");
        return NULL; //We can't search in an empty tree...
    }
    else{
        //hopefully works. Can we do anything if not? Should be an internal error then.
        // "Sollten interne Fehler auftreten, wenden Sie sich bitte an den Betreuer."
        return avl_get_data(root, key); //get the data and return it.
    }
}
static int set_value(uint32_t key, uint32_t value){
    DEBUG_PRINT("Setting Value of key '%d' to '%d' \n", key, value);
    if (root == NULL) {
        DEBUG_PRINT("No tree yet.\n", NULL);
    }
    else if (avl_get_data(root, key) != NULL) { //key exists.
        DEBUG_PRINT("Key already exists, deleting old one first.\n", NULL);
        
        callback_t del_function; //get our callback function as callback_t.
        del_function = cleanup;  //I think this is unneeded. Yep pretty sure.
                                 //But, if I delete this, nobody will read this comment.
        
        
        root = avl_delete_data(root, key, del_function, NULL); //Overwrite root with modified tree.
                                                          // - is this even a tree?
                                                          // is KVS stored in a tree?
                                                          // barely related Quote: "I'd like to be a tree."
    }
    else {
        DEBUG_PRINT("Key does not exists.\n", NULL);
        //Do nothing.
        //Such effect.
        //Very not.
        //Wait, stop. No. It should return something.
        //return -1; // Allright.
        //Nononono, don't return
        //You still need to add the new value below!
	}
    uint32_t *val_p; //where we want to malloc.
    val_p = malloc(sizeof(uint32_t)); 	//get storage. storage is nice. Should check if is NULL
    *val_p = value;						// which means it has failed. But then it can die anyway.
    DEBUG_PRINT("Value '%d' to put was at %p, is now stored at %p and still contains the value '%d'\n", value, &value, val_p, *val_p)
    root = avl_put_data(root, key, val_p); //fire it up into da' database.
    return 0;
}

static void cleanup(void * data, void * args){
    DEBUG_PRINT("Cleaning up at %p\n", data);
    if (data == NULL || &data == NULL) {
        printf("Invalid data to clean.");
        return;
    }
    free(data); //Free memory. Yay.
}
//------------------------------------------------------------//
void clean_up_tree(void){
    DEBUG_PRINT("Cleaning up the whole tree.\n", NULL);
    clean_up_avl(root, cleanup, NULL);
    root = NULL; //Quote from task: "Nach Durchlauf dieser Funktion ist es noetig,
                 // den Wurzelknoten (root) auf NULL zu setzen."
                 //Did that.
}
//------------------------------------------------------------//
static void print_data(void * data, void * args){
    if (data == NULL) {
        printf("Invalid data to print.");
        return;
    }
    uint32_t *hua; 		//Doing random stuff, not shure why this works.
    hua = data; 		//Doing random stuff, not shure why this works.
    printf("%d; ",*hua);	//Doing random stuff, not shure why this works.
    DEBUG_PRINT("Printed Data is located at %p\n", hua);
}
//------------------------------------------------------------//
//------------------------------------------------------------//
// Begin of IPC functions
//------------------------------------------------------------//
//------------------------------------------------------------//
int init_connection(void){
    printf("init_connection()");
    // put your code here
    return -1;
}
//------------------------------------------------------------//
int accept_connection(int accept_socket){
    printf("accept_connection()");
    // put your code here
    return -1;
}
//------------------------------------------------------------//
int handle_connection(int accepted_socket){
    printf("handle_connection()");
    // put your code here
    return -1;
}
//------------------------------------------------------------//
//------------------------------------------------------------//
// do not edit following lines, it is not necessary to do so.
//------------------------------------------------------------//
//------------------------------------------------------------//
#define TEST_VALUES 10
// This function is used to test all avl wrapper's.
// If everything works fine, this function returns 0
// otherwise -1.
int test_wrappers(void){
    set_value(255,255);
    preorder_work(root, print_data, NULL);
    DEBUG_PRINT("first part done.",NULL)
    printf("\n");
    
    set_value(255, 256); //It is storing numbers under a key which is the number?
    set_value(900, 900); // How inventive.
    preorder_work(root, print_data, NULL);
    DEBUG_PRINT("second part done.",NULL)
    printf("\n");
    
    for(uint32_t i = 0; i < TEST_VALUES; ++i){
        set_value(i, TEST_VALUES-i);
        preorder_work(root, print_data, NULL); //added for debug.
    }
    DEBUG_PRINT("second.2 part done.",NULL)
    preorder_work(root, print_data, NULL);
    printf("\n");
    DEBUG_PRINT("third part done.",NULL)
    
    for(uint32_t i = 0; i < TEST_VALUES; ++i){
        if(*get_value_by_key(i) != TEST_VALUES-i){
            printf("ERROR: key-value pair does not match!\n");
            return -1;
        }
        printf("Key value pair: O.K.\n");
    }
    //testing double clean up calls
    printf("testing clean up tree\n");
    clean_up_tree();
    printf("done\n");
    printf("testing repeated cleanup... \n");
    //repeated clean up call is done by main function
    return 0;
}
